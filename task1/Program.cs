﻿using Microsoft.Win32;
using System.Text;

internal class Program
{
  public  static void DisplayCurrentUserAutostart()
    {
        string path = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        Console.WriteLine($"Автозавантаження поточного користувача: {Environment.UserName}");

        using (RegistryKey currentUserKey = Registry.CurrentUser.OpenSubKey(path))
        {
            if (currentUserKey != null)
            {
                string[] currentAutorun = currentUserKey.GetValueNames();
                int val = 0;
                foreach (string program in currentAutorun)
                {
                    Console.WriteLine($"{++val}) " + program);
                }
            }
            else
            {
                Console.WriteLine("\nСписок автозавантаження поточного користувача порожній!\n");
            }
        }
    }

    public static void DisplayAllUsersAutostart()
    {
        string path = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

        string[] users = Registry.Users.GetSubKeyNames();
        foreach (string user in users)
        {
            try
            {
                using (RegistryKey key = Registry.Users.OpenSubKey(user + @"\" + path))
                {
                    if (key != null)
                    {
                        string[] values = key.GetValueNames();
                        if (values.Length > 0)
                        {
                            Console.WriteLine("Автозавантаження користувача " + user);

                            int val = 0;
                            foreach (string program in values)
                            {
                                Console.WriteLine($"{++val}) " + program);
                            }
                        }
                        else
                        {
                            Console.WriteLine($"\nСписок автозавантаження {user} порожній!\n");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
            }
        }
    }
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;
        DisplayCurrentUserAutostart();
        DisplayAllUsersAutostart();
    }
}