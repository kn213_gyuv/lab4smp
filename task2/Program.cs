﻿using Microsoft.Win32;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;
        string autorun = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        string winword = @"C:\Program Files\Microsoft Office\root\Office16\WINWORD.EXE";

        try
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(autorun, true);
            if (key != null)
            {
                key.SetValue("WinWord", $"\"{winword}\"");
                key.Close();
                Console.WriteLine($"Програма WinWord додана до списку автозапуску користувача {Environment.UserName}");
            }
            else
            {
                Console.WriteLine("Не вдалося отримати доступ до ключа реєстру.");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Виникла помилка: {ex.Message}");
        }
    }
}