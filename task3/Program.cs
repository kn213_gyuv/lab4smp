﻿using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
internal class Program
{
   
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;

        TaskService taskService = new TaskService();

        IEnumerable<Microsoft.Win32.TaskScheduler.Task> allTasks = taskService.AllTasks;

        Console.WriteLine("Завдання для усіх користувачів:");

        foreach (var task in allTasks)
        {
            Console.WriteLine($"Ім'я: {task.Name}, Статус: {task.State}, Остання активність: {task.LastRunTime}");
        }

        Console.WriteLine("\n-------------------------------\n");

        // Отримання списку завдань для поточного користувача
        //     TaskService currentUserTaskService = new TaskService();
        string currentUser = Environment.UserDomainName + "\\" + Environment.UserName;
        Console.WriteLine($"Поточний користувач: {currentUser}");

        TaskService currentUserTaskService = new TaskService();

        try
        {
            IEnumerable<Microsoft.Win32.TaskScheduler.Task> currentUserTasks = currentUserTaskService.AllTasks;

            Console.WriteLine("Завдання для поточного користувача:");

            foreach (var task in currentUserTasks)
            {
                Console.WriteLine($"Ім'я: {task.Name}, Статус: {task.State}, Остання активність: {task.LastRunTime}");
            }
        }
        catch (System.UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Помилка доступу: {ex.Message}");
        }
        catch (Exception e)
        {
            Console.WriteLine($"Помилка: {e.Message}");
        }



    }
}

