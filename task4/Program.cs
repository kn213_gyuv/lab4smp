﻿using Microsoft.Win32;
using System.Diagnostics;
using System.Text;

internal class Program
{
   
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;

        string savePath = "regex.reg";

        try
        {
            // Відкриваємо базовий ключ CurrentUser
            using (RegistryKey baseKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default))
            {
                // Отримуємо підключення до підкаталогу SOFTWARE\Microsoft\Windows\CurrentVersion\Run
                using (RegistryKey key = baseKey.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run"))
                {
                    if (key != null)
                    {
                        // Отримуємо імена значень реєстру
                        string[] valueNames = key.GetValueNames();

                        // Об'єднуємо імена значень у рядок
                        string regContent = string.Join(Environment.NewLine, valueNames);

                        // Зберігаємо вміст розділу реєстру у файл .reg
                        System.IO.File.WriteAllText(savePath, regContent);

                        Console.WriteLine("Розділ реєстру успішно скопійовано у файл .reg.");
                    }
                    else
                    {
                        Console.WriteLine("Розділ реєстру не знайдено.");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Сталася помилка: " + ex.Message);
        }
    }
}